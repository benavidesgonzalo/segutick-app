package com.main.segutick;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.main.segutick.data.conexion.SQLite.entity.SegutickDB;
import com.main.segutick.data.model.Evento;

import java.util.ArrayList;
import java.util.List;

public class LobbyActivity extends AppCompatActivity {

    ListView eventosLista;
    private SegutickDB SDB;
    List<Evento> eventos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_lobby);

        SDB = new SegutickDB(getApplicationContext());
        eventos = SDB.getAllEventos();

        List<String> Nombres = new ArrayList<>();
        for(Evento e : eventos){
            Nombres.add(e.getNombreEvento());
        }

        eventosLista = (ListView)findViewById(R.id.listEventos);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview, R.id.textView, Nombres);
        eventosLista.setAdapter(arrayAdapter);

        eventosLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long idEvento) {
                Log.i("EventosListView", "Seleccionado Item N° " + (idEvento) + " en la posición: " + i);
                // Then you start a new Activity via Intent
                Intent intent = new Intent(LobbyActivity.this,EventoActivity.class);
                intent.putExtra("idEvento", Long.valueOf(idEvento));
                startActivity(intent);
            }

        });

    }

}
