package com.main.segutick.data.conexion.SQLite.entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.main.segutick.data.conexion.Conexion;
import com.main.segutick.data.model.Asistente;
import com.main.segutick.data.model.Cliente;
import com.main.segutick.data.model.Entrada;
import com.main.segutick.data.model.Evento;
import com.main.segutick.data.model.TipoEntrada;
import com.main.segutick.data.model.TipoEvento;
import com.main.segutick.data.model.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class SegutickDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Segutick-DB";

    public static final String TABLE_TIPOEVENTOS = "tipoeventos";
    private static final String KEY_ID_TIPOEVENTO = "id";
    private static final String KEY_NOMBRE_TIPOEVENTO = "nombre_tipoevento";

    public static final String TABLE_ASISTENTE = "asistentes";
    private static final String KEY_ID_ASISTENTE = "id";
    private static final String KEY_RUT_ASISTENTE = "rut_asistente";
    private static final String KEY_NOMBRE_ASISTENTE = "nombre_asistente";
    private static final String KEY_APELLIDOPATERNO_ASISTENTE = "apellido_paterno_asistente";
    private static final String KEY_APELLIDOMATERNO_ASISTENTE = "apellido_materno_asistente";

    public static final String TABLE_TIPOENTRADAS = "tipoentradas";
    private static final String KEY_ID_ENTRADA = "id";
    private static final String KEY_DESCRIPCION_ENTRADA = "descripcion_tipoentrada";
    private static final String KEY_VALOR_ENTRADA = "valor_tipoentrada";
    private static final String KEY_EVENTO_FK_ENTRADA = "evento_tipoentrada";

    public static final String TABLE_CLIENTES = "clientes";
    private static final String KEY_ID_CLIENTE = "id";
    private static final String KEY_NOMBRE_CLIENTE = "nombre_clientes";

    public static final String TABLE_EVENTOS = "eventos";
    private static final String KEY_ID_EVENTO = "id";
    private static final String KEY_NOMBRE_EVENTO = "nombre_evento";
    private static final String KEY_TIPO_FK_EVENTO = "tipoevento_evento";

    public static final String TABLE_USUARIOS = "usuarios";
    private static final String KEY_ID_USUARIO = "id";
    private static final String KEY_RUT_USUARIO = "rut_usuario";
    private static final String KEY_CLAVE_USUARIO = "clave";
    private static final String KEY_NOMBRE_USUARIO = "nombre_usuario";
    private static final String KEY_APELLIDOPATERNO_USUARIO = "apellidopaterno_usuario";
    private static final String KEY_APELLIDOMATERNO_USUARIO = "apellidomaterno_usuario";

    public static final String TABLE_ENTRADA = "entradas";
    private static final String KEY_ID = "id";
    private static final String KEY_USADA = "usada";
    private static final String KEY_FK_TIPO_ENTRADA = "entradatipo_entrada";
    private static final String KEY_FK_ENTRADA_COMPRADOR = "entradacomprador_entrada";

    public SegutickDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CLIENTES_TABLE = "CREATE TABLE " + TABLE_CLIENTES + "("
                + KEY_ID_CLIENTE + " LONG PRIMARY KEY," + KEY_NOMBRE_CLIENTE + " TEXT" + ");";
        db.execSQL(CREATE_CLIENTES_TABLE);

        String CREATE_TIPOEVENTOS_TABLE = "CREATE TABLE " + TABLE_TIPOEVENTOS + "("
                + KEY_ID_TIPOEVENTO + " LONG PRIMARY KEY," + KEY_NOMBRE_TIPOEVENTO + " TEXT" + ");";
        db.execSQL(CREATE_TIPOEVENTOS_TABLE);

        String CREATE_EVENTOS_TABLE = "CREATE TABLE " + TABLE_EVENTOS + "("
                + KEY_ID_EVENTO + " LONG PRIMARY KEY," + KEY_NOMBRE_EVENTO + " TEXT,"
                + KEY_TIPO_FK_EVENTO + " LONG" + ");";
        db.execSQL(CREATE_EVENTOS_TABLE);

        String CREATE_TIPOENTRADAS_TABLE = "CREATE TABLE " + TABLE_TIPOENTRADAS + "("
                + KEY_ID_ENTRADA + " LONG PRIMARY KEY," + KEY_DESCRIPCION_ENTRADA + " TEXT,"
                + KEY_VALOR_ENTRADA + " INTEGER," + KEY_EVENTO_FK_ENTRADA + " LONG" + ");";
        db.execSQL(CREATE_TIPOENTRADAS_TABLE);

        String CREATE_ENTRADAS_TABLE = "CREATE TABLE " + TABLE_ENTRADA + "("
                + KEY_ID + " LONG PRIMARY KEY," + KEY_USADA + " BOOLEAN," + KEY_FK_ENTRADA_COMPRADOR + " LONG,"
                + KEY_FK_TIPO_ENTRADA + " LONG" + ");";
        db.execSQL(CREATE_ENTRADAS_TABLE);

        String CREATE_ASISTENTE_TABLE = "CREATE TABLE " + TABLE_ASISTENTE + "("
                + KEY_ID_ASISTENTE + " INTEGER PRIMARY KEY," + KEY_RUT_ASISTENTE + " TEXT UNIQUE,"
                + KEY_NOMBRE_ASISTENTE + " TEXT," + KEY_APELLIDOPATERNO_ASISTENTE + " TEXT,"
                + KEY_APELLIDOMATERNO_ASISTENTE + " TEXT " + ");";
        db.execSQL(CREATE_ASISTENTE_TABLE);

        String CREATE_USUARIOS_TABLE = "CREATE TABLE " + TABLE_USUARIOS + "(" +
                KEY_ID_USUARIO + " LONG PRIMARY KEY," + KEY_RUT_USUARIO + " TEXT," +
                KEY_NOMBRE_USUARIO + " TEXT," + KEY_APELLIDOPATERNO_USUARIO + " TEXT," +
                KEY_APELLIDOMATERNO_USUARIO + " TEXT"+ ");";
        db.execSQL(CREATE_USUARIOS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASISTENTE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRADA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIPOENTRADAS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIPOEVENTOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENTES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USUARIOS);

        onCreate(db);
    }

    // Upgrading database
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion, String TABLE) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE);
        // Create tables again
        onCreate(db);
    }

    // code to add the new usuario
    public void addUsuario(Usuario usuario) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_RUT_USUARIO, usuario.getRUT());

        // Inserting Row
        db.insert(TABLE_USUARIOS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get the single usuario
    public Usuario getUsuario(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_USUARIOS, new String[] { KEY_ID_USUARIO,
                        KEY_RUT_USUARIO, KEY_CLAVE_USUARIO, KEY_NOMBRE_USUARIO, KEY_APELLIDOPATERNO_USUARIO
                        , KEY_APELLIDOMATERNO_USUARIO}, KEY_ID_USUARIO + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Usuario usuario = new Usuario(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getString(4),cursor.getString(5));
        // return usuario
        return usuario;
    }

    // code to get all usuarios in a list view
    public List<Usuario> getAllUsuarios() {
        List<Usuario> usuarioList = new ArrayList<Usuario>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USUARIOS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Usuario usuario = new Usuario();
                usuario.setID(cursor.getLong(0));
                usuario.setRUT(cursor.getString(1));
                // Adding usuario to list
                usuarioList.add(usuario);
            } while (cursor.moveToNext());
        }

        // return usuario list
        return usuarioList;
    }

    // code to update the single usuario
    public int updateUsuario(Usuario usuario) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_USUARIO, usuario.getID());
        values.put(KEY_RUT_USUARIO, usuario.getRUT());

        // updating row
        return db.update(TABLE_USUARIOS, values, KEY_ID_USUARIO + " = ?",
                new String[] { String.valueOf(usuario.getID()) });
    }

    // Deleting single usuario
    public void deleteUsuario(Usuario usuario) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USUARIOS, KEY_ID_USUARIO + " = ?",
                new String[] { String.valueOf(usuario.getID()) });
        db.close();
    }

    // Getting usuarios Count
    public int getUsuariosCount() {
        String countQuery = "SELECT  * FROM " + TABLE_USUARIOS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


    // code to add the new tipoentrada
    public void addTipoEntrada(TipoEntrada tipoentrada) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, tipoentrada.getId());
        values.put(KEY_DESCRIPCION_ENTRADA, tipoentrada.getDescripcion());
        values.put(KEY_VALOR_ENTRADA, tipoentrada.getValor());
        values.put(KEY_EVENTO_FK_ENTRADA, tipoentrada.getEvento());

        // Inserting Row
        db.insert(TABLE_TIPOENTRADAS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get the single tipoentrada
    public TipoEntrada getTipoEntrada(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TIPOENTRADAS, new String[] { KEY_ID_ENTRADA,
                        KEY_DESCRIPCION_ENTRADA, KEY_VALOR_ENTRADA, KEY_EVENTO_FK_ENTRADA }, KEY_ID_ENTRADA + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        TipoEntrada tipoentrada = new TipoEntrada(cursor.getLong(0), cursor.getString(1),
                cursor.getInt(2), cursor.getLong(3));
        // return tipoentrada
        return tipoentrada;
    }

    public List<TipoEntrada> getAllTipoEntradasByEvento(Long id) {
        List<TipoEntrada> tipoentradaList = new ArrayList<TipoEntrada>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TIPOENTRADAS + " WHERE " + KEY_EVENTO_FK_ENTRADA + " = " +id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TipoEntrada tipoentrada = new TipoEntrada();
                tipoentrada.setId(cursor.getLong(0));
                tipoentrada.setDescripcion(cursor.getString(1));
                tipoentrada.setValor(cursor.getInt(2));
                tipoentrada.setEvento(cursor.getLong(3));
                // Adding tipoentrada to list
                tipoentradaList.add(tipoentrada);
            } while (cursor.moveToNext());
        }
        // return tipoentrada list
        return tipoentradaList;
    }

    // code to get all tipoentradas in a list view
    public List<TipoEntrada> getAllTipoEntradas() {
        List<TipoEntrada> tipoentradaList = new ArrayList<TipoEntrada>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TIPOENTRADAS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TipoEntrada tipoentrada = new TipoEntrada();
                tipoentrada.setId(cursor.getLong(0));
                tipoentrada.setDescripcion(cursor.getString(1));
                tipoentrada.setValor(cursor.getInt(2));
                tipoentrada.setEvento(cursor.getLong(3));
                // Adding tipoentrada to list
                tipoentradaList.add(tipoentrada);
            } while (cursor.moveToNext());
        }
        // return tipoentrada list
        return tipoentradaList;
    }

    // code to update the single tipoentrada
    public int updateTipoEntrada(TipoEntrada tipoentrada) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_ENTRADA, tipoentrada.getId());
        values.put(KEY_DESCRIPCION_ENTRADA, tipoentrada.getDescripcion());
        values.put(KEY_VALOR_ENTRADA, tipoentrada.getValor());
        values.put(KEY_EVENTO_FK_ENTRADA, tipoentrada.getEvento());

        // updating row
        return db.update(TABLE_TIPOENTRADAS, values, KEY_ID_ENTRADA + " = ?",
                new String[] { String.valueOf(tipoentrada.getId()) });
    }

    // Deleting single tipoentrada
    public void deleteTipoEntrada(TipoEntrada tipoentrada) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TIPOENTRADAS, KEY_ID_ENTRADA + " = ?",
                new String[] { String.valueOf(tipoentrada.getId()) });
        db.close();
    }

    // Getting tipoentradas Count
    public int getTipoEntradasCount() {
        String countQuery = "SELECT  * FROM " + TABLE_TIPOENTRADAS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    // code to add the new asistente
    public void addAsistente(Asistente asistente) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_ASISTENTE, asistente.getId());
        values.put(KEY_RUT_ASISTENTE, asistente.getRUT());
        values.put(KEY_NOMBRE_ASISTENTE, asistente.getNombre());
        values.put(KEY_APELLIDOPATERNO_ASISTENTE, asistente.getApellidoPaterno());
        values.put(KEY_APELLIDOMATERNO_ASISTENTE, asistente.getApellidoMaterno());

        // Inserting Row  
        db.insert(TABLE_ASISTENTE, null, values);
        //2nd argument is String containing nullColumnHack  
        db.close(); // Closing database connection  
    }

    // code to get the single asistente  
    public Asistente getAsistente(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Asistente asistente = new Asistente();
        Cursor cursor = db.query(TABLE_ASISTENTE, new String[] { KEY_ID_ASISTENTE,
                        KEY_RUT_ASISTENTE, KEY_NOMBRE_ASISTENTE, KEY_APELLIDOPATERNO_ASISTENTE, KEY_APELLIDOMATERNO_ASISTENTE }, KEY_ID_ASISTENTE + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            asistente = new Asistente(cursor.getString(1),cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getLong(0));
        }


        // return asistente  
        return asistente;
    }

    // code to get the single asistente
    public List<Asistente> getAsistentesByEvento(Long id) {
        Long idEvento = Long.valueOf(id);
        SQLiteDatabase db = this.getReadableDatabase();
        List<TipoEntrada> ten = this.getAllTipoEntradasByEvento(idEvento);
        List<Entrada> en = new ArrayList<>();
        for(TipoEntrada t : ten){
             en.addAll(this.getAllEntradasByTipo(t.getId()));
        }

        List<Asistente> las = new ArrayList<>();
        for(Entrada e : en){
            las.add(this.getAsistente(e.getComprador()));
        }
        return las;
    }

    public Asistente getAsistente(String rut) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ASISTENTE, new String[] { KEY_ID_ASISTENTE,
                        KEY_RUT_ASISTENTE, KEY_NOMBRE_ASISTENTE, KEY_APELLIDOPATERNO_ASISTENTE, KEY_APELLIDOMATERNO_ASISTENTE }, KEY_RUT_ASISTENTE + "=?",
                new String[] { rut }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Asistente asistente = new Asistente(cursor.getString(1),cursor.getString(2),
                cursor.getString(3), cursor.getString(4), cursor.getLong(0));
        // return asistente
        return asistente;
    }

    // code to get all asistentes in a list view  
    public List<Asistente> getAllAsistentes() {
        List<Asistente> asistenteList = new ArrayList<Asistente>();
        // Select All Query  
        String selectQuery = "SELECT  * FROM " + TABLE_ASISTENTE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {
            do {
                Asistente asistente = new Asistente();
                asistente.setId(cursor.getLong(0));
                asistente.setRUT(cursor.getString(1));
                asistente.setNombre(cursor.getString(2));
                asistente.setApellidoPaterno(cursor.getString(3));
                asistente.setApellidoMaterno(cursor.getString(4));
                // Adding asistente to list
                asistenteList.add(asistente);
            } while (cursor.moveToNext());
        }

        // return asistente list  
        return asistenteList;
    }

    // code to update the single asistente  
    public int updateAsistente(Asistente asistente) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_ASISTENTE, asistente.getId());
        values.put(KEY_RUT_ASISTENTE, asistente.getRUT());
        values.put(KEY_NOMBRE_ASISTENTE, asistente.getNombre());
        values.put(KEY_APELLIDOPATERNO_ASISTENTE, asistente.getApellidoPaterno());
        values.put(KEY_APELLIDOMATERNO_ASISTENTE, asistente.getApellidoMaterno());

        // updating row  
        return db.update(TABLE_ASISTENTE, values, KEY_ID_ASISTENTE + " = ?",
                new String[] { String.valueOf(asistente.getId()) });
    }

    // Deleting single asistente  
    public void deleteAsistente(Asistente asistente) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ASISTENTE, KEY_ID_ASISTENTE + " = ?",
                new String[] { String.valueOf(asistente.getId()) });
        db.close();
    }

    // Getting asistentes Count  
    public int getAsistentesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ASISTENTE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count  
        return cursor.getCount();
    }

    // code to add the new cliente
    public void addCliente(Cliente cliente) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NOMBRE_CLIENTE, cliente.getNombre());

        // Inserting Row
        db.insert(TABLE_CLIENTES, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get the single cliente
    public Cliente getCliente(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CLIENTES, new String[] { KEY_ID_CLIENTE,
                        KEY_NOMBRE_CLIENTE }, KEY_ID_CLIENTE + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Cliente cliente = new Cliente(cursor.getLong(0), cursor.getString(1));
        // return cliente
        return cliente;
    }

    // code to get all clientes in a list view
    public List<Cliente> getAllClientes() {
        List<Cliente> clienteList = new ArrayList<Cliente>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CLIENTES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Cliente cliente = new Cliente();
                cliente.setID(cursor.getLong(0));
                cliente.setNombre(cursor.getString(1));
                // Adding cliente to list
                clienteList.add(cliente);
            } while (cursor.moveToNext());
        }

        // return cliente list
        return clienteList;
    }

    // code to update the single cliente
    public int updateCliente(Cliente cliente) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_CLIENTE, cliente.getID());
        values.put(KEY_NOMBRE_CLIENTE, cliente.getNombre());

        // updating row
        return db.update(TABLE_CLIENTES, values, KEY_ID_CLIENTE + " = ?",
                new String[] { String.valueOf(cliente.getID()) });
    }

    // Deleting single cliente
    public void deleteCliente(Cliente cliente) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CLIENTES, KEY_ID_CLIENTE + " = ?",
                new String[] { String.valueOf(cliente.getID()) });
        db.close();
    }

    // Getting clientes Count
    public int getClientesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CLIENTES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    // code to add the new evento
    public void addEvento(Evento evento) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_EVENTO, evento.getId());
        values.put(KEY_NOMBRE_EVENTO, evento.getNombreEvento());
        values.put(KEY_TIPO_FK_EVENTO, evento.getTipoEvento());

        // Inserting Row
        db.insert(TABLE_EVENTOS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get the single evento
    public Evento getEvento(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Evento evento = new Evento();
        Cursor cursor = db.query(TABLE_EVENTOS, new String[] { KEY_ID_EVENTO,
                        KEY_NOMBRE_EVENTO, KEY_TIPO_FK_EVENTO }, KEY_ID_EVENTO + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        if(cursor.moveToFirst()) {
            evento = new Evento(cursor.getLong(0), cursor.getString(1), cursor.getLong(2));
        }
        // return evento
        return evento;
    }

    // code to get all eventos in a list view
    public List<Evento> getAllEventos() {
        List<Evento> eventoList = new ArrayList<Evento>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EVENTOS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Evento evento = new Evento();
                evento.setId(cursor.getLong(0));
                evento.setNombreEvento(cursor.getString(1));
                evento.setTipoEvento(cursor.getLong(2));
                // Adding evento to list
                eventoList.add(evento);
            } while (cursor.moveToNext());
        }

        // return evento list
        return eventoList;
    }

    // code to update the single evento
    public int updateEvento(Evento evento) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_EVENTO, evento.getId());
        values.put(KEY_NOMBRE_EVENTO, evento.getNombreEvento());
        values.put(KEY_TIPO_FK_EVENTO, evento.getTipoEvento());

        // updating row
        return db.update(TABLE_EVENTOS, values, KEY_ID_EVENTO + " = ?",
                new String[] { String.valueOf(evento.getId()) });
    }

    // Deleting single evento
    public void deleteEvento(Evento evento) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EVENTOS, KEY_ID_EVENTO + " = ?",
                new String[] { String.valueOf(evento.getId()) });
        db.close();
    }

    // Getting eventos Count
    public int getEventosCount() {
        String countQuery = "SELECT  * FROM " + TABLE_EVENTOS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    public void addEntrada(Entrada entrada) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, entrada.getId());
        values.put(KEY_USADA, false);
        values.put(KEY_FK_TIPO_ENTRADA, entrada.getTipoEntrada());
        values.put(KEY_FK_ENTRADA_COMPRADOR, entrada.getComprador());

        // Inserting Row
        db.insert(TABLE_ENTRADA, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    public Boolean validarEntrada(String  rut, Long Evento){
        SQLiteDatabase db = this.getReadableDatabase();
        Boolean validar = false;
        List<Asistente> las = this.getAsistentesByEvento(Evento);

        for(Asistente a : las){
            if(a.getRUT().equals(rut.trim()))
                validar = true;
        }

        return validar;
    }
    
    // code to get the single entrada
    public Entrada getEntrada(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ENTRADA, new String[] { KEY_ID,
                        KEY_FK_TIPO_ENTRADA, KEY_FK_ENTRADA_COMPRADOR, KEY_USADA }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Boolean uso = cursor.getInt(3) > 0;
        Entrada entrada = new Entrada(cursor.getLong(0),cursor.getLong(2),
                cursor.getLong(1), uso);
        // return entrada
        return entrada;
    }

    // code to get all entradas in a list view
    public List<Entrada> getAllEntradas() {
        List<Entrada> entradaList = new ArrayList<Entrada>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ENTRADA;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Entrada entrada = new Entrada();
                entrada.setId(cursor.getLong(0));
                boolean value = cursor.getInt(1) > 0;
                entrada.setUsada(value);
                entrada.setComprador(cursor.getLong(2));
                entrada.setTipoEntrada(cursor.getLong(3));
                // Adding entrada to list
                entradaList.add(entrada);
            } while (cursor.moveToNext());
        }

        // return entrada list
        return entradaList;
    }

    public List<Entrada> getAllEntradasByRut(String RUT) {
        List<Entrada> entradaList = new ArrayList<Entrada>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ENTRADA + " WHERE " + KEY_FK_ENTRADA_COMPRADOR + " = " + RUT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Entrada entrada = new Entrada();
                entrada.setId(cursor.getLong(0));
                boolean value = cursor.getInt(1) > 0;
                entrada.setUsada(value);
                entrada.setComprador(cursor.getLong(2));
                entrada.setTipoEntrada(cursor.getLong(3));
                // Adding entrada to list
                entradaList.add(entrada);
            } while (cursor.moveToNext());
        }

        // return entrada list
        return entradaList;
    }

    public List<Entrada> getAllEntradasByTipo(Long id) {
        List<Entrada> entradaList = new ArrayList<Entrada>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ENTRADA + " WHERE " + KEY_FK_TIPO_ENTRADA + " = " + id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Entrada entrada = new Entrada();
                entrada.setId(cursor.getLong(0));
                boolean value = cursor.getInt(1) > 0;
                entrada.setUsada(value);
                entrada.setComprador(cursor.getLong(2));
                entrada.setTipoEntrada(cursor.getLong(3));
                // Adding entrada to list
                entradaList.add(entrada);
            } while (cursor.moveToNext());
        }

        // return entrada list
        return entradaList;
    }

    // code to update the single entrada
    public int updateEntrada(Entrada entrada) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, entrada.getId());
        values.put(KEY_USADA, entrada.getUsada());
        values.put(KEY_FK_TIPO_ENTRADA, entrada.getTipoEntrada());
        values.put(KEY_FK_ENTRADA_COMPRADOR, entrada.getComprador());

        // updating row
        return db.update(TABLE_ENTRADA, values, KEY_ID + " = ?",
                new String[] { String.valueOf(entrada.getId()) });
    }

    // Deleting single entrada
    public void deleteEntrada(Entrada entrada) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ENTRADA, KEY_ID + " = ?",
                new String[] { String.valueOf(entrada.getId()) });
        db.close();
    }

    // Getting entradas Count
    public int getEntradasCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ENTRADA;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


    // code to add the new tipoevento
    public void addTipoEvento(TipoEvento tipoevento) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, tipoevento.getId());
        values.put(KEY_NOMBRE_TIPOEVENTO, tipoevento.getNombreEvento());

        // Inserting Row
        db.insert(TABLE_TIPOEVENTOS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get the single tipoevento
    public TipoEvento getTipoEvento(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TIPOEVENTOS, new String[] { KEY_ID_TIPOEVENTO,
                        KEY_NOMBRE_TIPOEVENTO }, KEY_ID_TIPOEVENTO + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        TipoEvento tipoevento = new TipoEvento(cursor.getLong(0), cursor.getString(1));
        // return tipoevento
        return tipoevento;
    }

    // code to get all tipoeventos in a list view
    public List<TipoEvento> getAllTipoEventos() {
        List<TipoEvento> tipoeventoList = new ArrayList<TipoEvento>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TIPOEVENTOS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TipoEvento tipoevento = new TipoEvento();
                tipoevento.setId(cursor.getLong(0));
                tipoevento.setNombreEvento(cursor.getString(1));
                // Adding tipoevento to list
                tipoeventoList.add(tipoevento);
            } while (cursor.moveToNext());
        }

        // return tipoevento list
        return tipoeventoList;
    }

    // code to update the single tipoevento
    public int updateTipoEvento(TipoEvento tipoevento) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_TIPOEVENTO, tipoevento.getId());
        values.put(KEY_NOMBRE_TIPOEVENTO, tipoevento.getNombreEvento());

        // updating row
        return db.update(TABLE_TIPOEVENTOS, values, KEY_ID_TIPOEVENTO + " = ?",
                new String[] { String.valueOf(tipoevento.getId()) });
    }

    // Deleting single tipoevento
    public void deleteTipoEvento(TipoEvento tipoevento) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TIPOEVENTOS, KEY_ID_TIPOEVENTO + " = ?",
                new String[] { String.valueOf(tipoevento.getId()) });
        db.close();
    }

    // Getting tipoeventos Count
    public int getTipoEventosCount() {
        String countQuery = "SELECT  * FROM " + TABLE_TIPOEVENTOS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }
}

