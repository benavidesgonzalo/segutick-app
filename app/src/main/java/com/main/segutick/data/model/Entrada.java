package com.main.segutick.data.model;

import android.content.Context;

import com.main.segutick.data.conexion.Conexion;
import com.main.segutick.data.conexion.SQLite.entity.SegutickDB;

import java.net.ContentHandler;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Entrada {
    private Long id;
    private Long comprador;
    private Long tipoEntrada;
    private Boolean usada;

    public Entrada(Long rutComprador, Long tipoEntrada) {
        this.comprador = rutComprador;
        this.tipoEntrada = tipoEntrada;
        this.usada = false;
    }

    public Entrada(Long id, Long rutComprador, Long tipoEntrada) {
        this.id = id;
        this.comprador = rutComprador;
        this.tipoEntrada = tipoEntrada;
        this.usada = false;
    }

    public Entrada(Long id, Long rutComprador, Long tipoEntrada, Boolean usada) {
        this.id = id;
        this.comprador = rutComprador;
        this.tipoEntrada = tipoEntrada;
        this.usada = usada;
    }

    public Entrada() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(Long tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

    public Long getComprador() {
        return comprador;
    }

    public void setComprador(Long comprador) {
        this.comprador = comprador;
    }

    public Boolean getUsada() {
        return usada;
    }

    public void setUsada(Boolean usada) {
        this.usada = usada;
    }

    public static List<Entrada> generar(){
        Long[] tipoentradas = {0L,0L,0L,1L,1L,1L,2L,2L,2L};
        Long[] compradores =  {0L,1L,2L,3L,4L,5L,6L,7L,8L};
        List<Entrada> len = new ArrayList<>();
        for(Integer i = 0 ; i < compradores.length ; i++){
            Entrada e = new Entrada(Long.valueOf(i),compradores[i], tipoentradas[i]);
            e.setUsada(false);
            len.add(e);
        }
        return len;
    }
}
