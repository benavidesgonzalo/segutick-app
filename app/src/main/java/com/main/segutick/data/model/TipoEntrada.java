package com.main.segutick.data.model;

import com.main.segutick.data.conexion.Conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TipoEntrada {
    private Long id;
    private String descripcion;
    private Integer valor;
    private Long evento;

    public TipoEntrada() {
    }

    public TipoEntrada(Long id, String descripcion, Integer valor, Long evento) {
        this.id = id;
        this.descripcion = descripcion;
        this.valor = valor;
        this.evento = evento;
    }

    public TipoEntrada(String descripcion, Integer valor, Long evento) {
        this.id = id;
        this.descripcion = descripcion;
        this.valor = valor;
        this.evento = evento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public Long getEvento() {
        return evento;
    }

    public void setEvento(Long evento) {
        this.evento = evento;
    }

    public boolean addTipoEntrada(String descripcion, Integer valor, Evento ev){
        boolean valido = false;
        try{
            Connection conn = Conexion.Conectar();
            String SQL = "INSERT INTO TIPO_ENTRADA(tipoEntradaDescripcion,valorEntrada,entradaEvento)"
                    + "values(?,?,?)";
            PreparedStatement psql = conn.prepareStatement(SQL);
            psql.setString(0, descripcion);
            psql.setInt(1, valor);
            psql.setLong(2, ev.getId());
            psql.execute();
            valido = true;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return valido;
    }

    public List<TipoEntrada> findAll(){
        List<TipoEntrada> ListE = new ArrayList<TipoEntrada>();
        try{
            Connection conn = Conexion.Conectar();
            String SQL = "SELECT * FROM TIPO_EVENTO";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            while(rs.next()){
                TipoEntrada E = new TipoEntrada();
                E.setId(rs.getLong("id"));
                E.setValor(rs.getInt("valorEntrada"));
                E.setDescripcion(rs.getString("tipoentradadescripcion"));
                E.setEvento(rs.getLong("entradaevento"));
                ListE.add(E);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return ListE;
    }

    public TipoEntrada findById(Long id){
        TipoEntrada TE =  new TipoEntrada();
        try{
            Connection conn = Conexion.Conectar();
            String SQL = "SELECT * FROM TIPO_EVENTO where id=?";
            PreparedStatement pst = conn.prepareStatement(SQL);
            pst.setLong(1,id);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                TE.setId(rs.getLong("id"));
                TE.setValor(rs.getInt("valorEntrada"));
                TE.setDescripcion(rs.getString("tipoentradadescripcion"));
                TE.setEvento(rs.getLong("entradaevento"));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return TE;
    }

    public static List<TipoEntrada> generar(){
        List<TipoEntrada> lte = new ArrayList<>();
        String[] ste = {"Sala A","Cancha","Platea"};
        Integer[] ite = {4000,22000,17000};
        for(Integer i = 0 ; i < ste.length ; i++){
            TipoEntrada te = new TipoEntrada(Long.valueOf(i),ste[i], ite[i], Long.valueOf(i));
            lte.add(te);
        }
        return lte;
    }
}



