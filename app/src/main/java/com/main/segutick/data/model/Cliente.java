package com.main.segutick.data.model;

import java.util.ArrayList;
import java.util.List;

public class Cliente {

    private Long ID;
    private String Nombre;

    public Cliente() {
    }

    public Cliente(Long ID, String Nombre) {
        this.ID = ID;
        this.Nombre = Nombre;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public static List<Cliente> generar(){
        List<Cliente> LC = new ArrayList<>();
        String[] sl = {"ENTEL","FIFA","CINEHOYTS"};
        for(Integer i = 0 ; i < 3 ; i++) {
            Cliente e = new Cliente(i.longValue(), sl[i]);
            LC.add(e);
        }
        return LC;
    }

}
