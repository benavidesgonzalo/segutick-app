package com.main.segutick.data.model;

import android.content.Context;

import com.main.segutick.data.conexion.Conexion;
import com.main.segutick.data.conexion.SQLite.entity.SegutickDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Asistente {
    private String RUT;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombre;
    private Long id;

    public Asistente(String RUT, String apellidoPaterno, String apellidoMaterno, String nombre, Long id) {
        this.RUT = RUT;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.nombre = nombre;
        this.id = id;
    }

    public Asistente(String RUT, String apellidoPaterno, String apellidoMaterno, String nombre) {
        this.RUT = RUT;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.nombre = nombre;
     }

    public Asistente(String RUT, Long id) {
        this.RUT = RUT;
        this.id = id;
    }

    public Asistente(String RUT) {
        this.RUT = RUT;
    }

    public Asistente(Long id) {
        this.id = id;
    }
    public Asistente() {
    }

    public String getRUT() {
        return RUT;
    }

    public void setRUT(String RUT) {
        this.RUT = RUT;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static List<Asistente> generar(){
        List<Asistente> la = new ArrayList<>();
        String[] ruts = {"11453110-3","9059199-2","18934424-4",
                         "20023287-9","14002618-0","13233780-6",
                         "17309102-K","10779634-7", "18796386-9"};
        int i = 0;
        for(String e : ruts){
            Asistente aux = new Asistente(e,"","","",Long.valueOf(i));
            la.add(aux);
            i++;
        }
        return la;
    }

}

