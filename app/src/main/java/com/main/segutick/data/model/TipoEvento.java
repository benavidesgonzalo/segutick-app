package com.main.segutick.data.model;

import com.main.segutick.data.conexion.Conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TipoEvento {
    private Long Id;
    private String nombreEvento;

    public TipoEvento() {
    }

    public TipoEvento(Long Id, String nombreEvento) {
        this.Id = Id;
        this.nombreEvento = nombreEvento;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getNombreEvento() {
        return nombreEvento;
    }

    public void setNombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
    }

    public List<TipoEvento> findAll(){
        List<TipoEvento> ListE = new ArrayList<TipoEvento>();
        try{
            Connection conn = Conexion.Conectar();
            String SQL = "SELECT * FROM TIPO_EVENTO";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            while(rs.next()){
                TipoEvento E = new TipoEvento();
                E.setId(rs.getLong("id"));
                E.setNombreEvento(rs.getString("nombretipoevento"));
                ListE.add(E);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return ListE;
    }

    public boolean addTipoEvento(String nombre){
        boolean valido = false;
        try{
            Connection conn = Conexion.Conectar();
            String SQL = "INSERT INTO TIPO_EVENTO(nombreTipoEvento)"
                    + "values(?)";
            PreparedStatement psql = conn.prepareStatement(SQL);
            psql.setString(0, nombre);
            psql.execute();
            valido = true;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return valido;
    }

    public TipoEvento findById(Long id){
        TipoEvento TE = new TipoEvento();
        try{
            Connection conn = Conexion.Conectar();
            String SQL = "SELECT * FROM TIPO_EVENTO where id=?";
            PreparedStatement pst = conn.prepareStatement(SQL);
            pst.setLong(1,id);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                TE.setId(rs.getLong("id"));
                TE.setNombreEvento(rs.getString("nombretipoevento"));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return TE;
    }

    public static List<TipoEvento> generar(){
        List<TipoEvento> lte = new ArrayList<>();
        String[] ste = {"Cine","Concierto","Recital"};
        for(Integer i = 0 ; i < ste.length ; i++){
            TipoEvento te = new TipoEvento(Long.valueOf(i), ste[i]);
            lte.add(te);
        }
        return lte;
    }
}

