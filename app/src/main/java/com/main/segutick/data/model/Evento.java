package com.main.segutick.data.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.main.segutick.data.conexion.Conexion;
import com.main.segutick.data.conexion.SQLite.entity.SegutickDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Evento {
    private Long id;
    private String nombreEvento;
    private Long tipoEvento;

    public Evento() {
    }

    public Evento(Long id, String nombreEvento, Long tipoEvento) {
        this.id = id;
        this.nombreEvento = nombreEvento;
        this.tipoEvento = tipoEvento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEvento() {
        return nombreEvento;
    }

    public void setNombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
    }

    public Long getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(Long tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public static List<Evento> findAll(Context context){
        List<Evento> ListE = new ArrayList<Evento>();
        try{
            SegutickDB SDB = new SegutickDB(context);
            for(Evento e : SDB.getAllEventos())
            {
                ListE.add(e);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return ListE;

    }

    public static List<Evento> generar(){
        List<Evento> le = new ArrayList<>();
        String[] sle = {"Los Simpson, la película", "Metallica! en Chile", "Denise Rosenthal en vivo"};
        for(Integer i = 0 ; i < sle.length; i++){
            Evento e = new Evento( Long.valueOf(i), sle[i], Long.valueOf(i) );
            le.add(e);
        }
        return le;
    }
}
