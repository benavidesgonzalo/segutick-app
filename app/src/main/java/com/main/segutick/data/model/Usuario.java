package com.main.segutick.data.model;

import java.util.ArrayList;
import java.util.List;

public class Usuario {

    private Long ID;
    private String clavehash;
    private String RUT;
    private String nombre;
    private String apellidopaterno;
    private String apellidomaterno;

    public Usuario() {
    }

    public Usuario(Long ID, String RUT,String clave) {
        this.ID = ID;
        this.RUT = RUT;
        this.clavehash = clave;
    }

    public Usuario(Long ID, String clavehash, String RUT, String nombre, String apellidopaterno, String apellidomaterno) {
        this.ID = ID;
        this.clavehash = clavehash;
        this.RUT = RUT;
        this.nombre = nombre;
        this.apellidopaterno = apellidopaterno;
        this.apellidomaterno = apellidomaterno;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getRUT() {
        return RUT;
    }

    public void setRUT(String RUT) {
        this.RUT = RUT;
    }

    public String getClavehash() {
        return clavehash;
    }

    public void setClavehash(String clavehash) {
        this.clavehash = clavehash;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidopaterno() {
        return apellidopaterno;
    }

    public void setApellidopaterno(String apellidopaterno) {
        this.apellidopaterno = apellidopaterno;
    }

    public String getApellidomaterno() {
        return apellidomaterno;
    }

    public void setApellidomaterno(String apellidomaterno) {
        this.apellidomaterno = apellidomaterno;
    }

    public static boolean validarRut(String rut) {

        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

    static public List<String> getRUTs(){
        List<String> EString = new ArrayList<String>();
        for(int i = 0 ; i < 2 ; i++){
            EString.add("18796386-9");
            EString.add("1-9");
        }
        return EString;
    }

    public static  Usuario generar(){
        Usuario u = new Usuario(0L,"18796386-9","Server123.","Gonzalo","Benavides","Coloma");
        return u;
    }
}
