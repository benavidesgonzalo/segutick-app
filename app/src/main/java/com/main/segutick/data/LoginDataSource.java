package com.main.segutick.data;

import com.main.segutick.data.model.LoggedInUser;
import com.main.segutick.data.model.Usuario;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            if(Usuario.validarRut(username) ) {
                LoggedInUser fakeUser =
                        new LoggedInUser(
                                java.util.UUID.randomUUID().toString(),
                                "");
                return new Result.Success(fakeUser);
            }
            else
                throw new Exception();
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
