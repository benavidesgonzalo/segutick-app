package com.main.segutick;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.google.android.material.textfield.TextInputEditText;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.main.segutick.data.conexion.SQLite.entity.SegutickDB;
import com.main.segutick.data.model.Asistente;
import com.main.segutick.data.model.Entrada;
import com.main.segutick.data.model.Evento;

import java.util.ArrayList;
import java.util.List;

public class EventoActivity extends AppCompatActivity {

    private Long idEvento;
    private Button btnQR;
    private Button btnRUT;
    private IntentIntegrator qrScanner;
    private TextView txtEvento;
    private TextInputEditText txtRUT;
    private SegutickDB SDB;
    ListView eventosLista2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento);

        SDB = new SegutickDB(getApplicationContext());

        Intent intent = getIntent();
        txtEvento = (TextView) findViewById(R.id.txtEvento);
        qrScanner = new IntentIntegrator(this);
        qrScanner.setOrientationLocked(true);

        idEvento = intent.getLongExtra("idEvento",-1L);
        btnQR  = (Button) findViewById(R.id.btnQRscan);
        btnRUT = (Button) findViewById(R.id.btnRUTscan);
        txtRUT = (TextInputEditText) findViewById(R.id.inputRUT);

        Evento este = SDB.getEvento(idEvento);
        txtEvento.setText(este.getNombreEvento());

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1011);

        btnRUT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String RUN = txtRUT.getText().toString().trim();
                Intent IMG = new Intent(EventoActivity.this, okerror_img.class);
                System.out.println("RUN");

                Entrada Ent = new Entrada();
                if(SDB.validarEntrada(RUN, idEvento))
                    IMG.putExtra("Validacion", true);
                else
                    IMG.putExtra("Validacion", false);
                startActivity(IMG);
            }
        });


        btnQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Abriendo escáner QR", Toast.LENGTH_SHORT).show();
                qrScanner.initiateScan();
            }
        });

        List<String> rut = new ArrayList<>();
        for(Asistente e : SDB.getAsistentesByEvento(idEvento)){
            rut.add(e.getRUT());
        }

        eventosLista2 = (ListView)findViewById(R.id.listEventos2);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview_2, R.id.textView, rut);
        eventosLista2.setAdapter(arrayAdapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null){
            if(result.getContents() == null){
                Toast.makeText(this, "QR Vacío", Toast.LENGTH_SHORT).show();
            } else {
                try{
                        String QR = result.getContents().toString();
                        String RUN = QR.substring(QR.indexOf("=")+1, QR.indexOf("&"));

                        Intent IMG = new Intent(EventoActivity.this, okerror_img.class);
                        Entrada ent = new Entrada();
                        if(SDB.validarEntrada(RUN, idEvento))
                            IMG.putExtra("Validacion", true);
                        else
                            IMG.putExtra("Validacion", false);
                        startActivity(IMG);
                        Toast.makeText(this, "RUN: " + RUN, Toast.LENGTH_LONG).show();

                }
                catch(Exception e){
                    e.printStackTrace();
                    Toast.makeText(this, "ERROR QR", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
}
