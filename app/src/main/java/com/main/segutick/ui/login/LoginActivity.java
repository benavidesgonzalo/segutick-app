package com.main.segutick.ui.login;

import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.main.segutick.LobbyActivity;
import com.main.segutick.R;
import com.main.segutick.data.conexion.SQLite.entity.SegutickDB;
import com.main.segutick.data.model.Asistente;
import com.main.segutick.data.model.Cliente;
import com.main.segutick.data.model.Entrada;
import com.main.segutick.data.model.Evento;
import com.main.segutick.data.model.TipoEntrada;
import com.main.segutick.data.model.TipoEvento;

import java.sql.SQLOutput;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;
    private SegutickDB SDB;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        SDB = new SegutickDB(getApplicationContext());
        this.generarBD();

        final EditText passwordEditText = findViewById(R.id.password);
        final EditText usernameEditText = findViewById(R.id.username);
        final Button loginButton = findViewById(R.id.login);

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                if (loginResult.getError() != null)
                    showLoginFailed(loginResult.getError());
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                    System.out.println("Login Correcto");
                    startActivity(new Intent(LoginActivity.this, LobbyActivity.class));
                }
                setResult(Activity.RESULT_OK);

                //Complete and destroy login activity once successful
                finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };

        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        });


    }


    private void generarBD(){
        try {
            Toast.makeText(LoginActivity.this, "Creando base de datos...", Toast.LENGTH_SHORT).show();
            SQLiteDatabase db = SDB.getWritableDatabase();
            SDB.onUpgrade(db, 1, 2);
            for (Cliente c : Cliente.generar()) {
                SDB.addCliente(c);
            }
            for (TipoEvento t : TipoEvento.generar()) {
                SDB.addTipoEvento(t);
            }
            for (Evento e : Evento.generar()) {
                SDB.addEvento(e);
            }
            for (TipoEntrada te : TipoEntrada.generar()){
                SDB.addTipoEntrada(te);
            }
            for(Entrada en : Entrada.generar()) {
                SDB.addEntrada(en);
            }
            for(Asistente as : Asistente.generar()) {
                SDB.addAsistente(as);
            }
            Toast.makeText(LoginActivity.this, "Base de datos creada...", Toast.LENGTH_SHORT).show();
        }
        catch(Exception e){
            Toast.makeText(LoginActivity.this, "Hubo un error, revisar log", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();

    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}
