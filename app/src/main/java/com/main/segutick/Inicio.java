package com.main.segutick;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;

import com.main.segutick.data.conexion.Conexion;
import com.main.segutick.data.conexion.SQLite.entity.SegutickDB;
import com.main.segutick.ui.login.LoginActivity;

public class Inicio extends AppCompatActivity {
    Conexion con;
    private SegutickDB SDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageButton logo = (ImageButton) findViewById(R.id.logoBtn);

        SDB = new SegutickDB(getApplicationContext());

        Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
           @Override
            public void run(){
               Toast.makeText(getApplicationContext(), "Bienvenido a Segutick", Toast.LENGTH_LONG).show();
               startActivity(new Intent(Inicio.this, LoginActivity.class));
           }
        }, 3000);

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Inicio.this, LoginActivity.class));
            }
        });
    }

    String message = null;
    String conmsg = null;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
