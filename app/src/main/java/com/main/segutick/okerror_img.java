package com.main.segutick;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class okerror_img extends AppCompatActivity {

    private ImageView mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_okerror_img);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.5), (int)(height*.5));
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;
//        params.alpha = 0;
        getWindow().setAttributes(params);

        Intent intent = getIntent();
        Boolean OK = intent.getBooleanExtra("Validacion", false);
        if(OK) {
            mDialog = (ImageView) findViewById(R.id.ok);
            mDialog.setVisibility(View.VISIBLE);
            mDialog = (ImageView) findViewById(R.id.error);
            mDialog.setVisibility(View.INVISIBLE);
        }
        else {
            mDialog = (ImageView) findViewById(R.id.error);
            mDialog.setVisibility(View.VISIBLE);
            mDialog = (ImageView) findViewById(R.id.ok);
            mDialog.setVisibility(View.INVISIBLE);
        }
        mDialog.setClickable(true);
        mDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
